extends CharacterBody2D
class_name Chicken

@onready var _animated_sprite = $AnimatedSprite2D
@onready var ai=$AI

func _ready():
	_animated_sprite.play("idle")
	ai.initialize(self)

func _process(_delta):
	playAnim()
	match ai.get_state():
		0:
			_animated_sprite.play("idle")
		1:
			_animated_sprite.play("walk")

func setup(x, y):
	position = Vector2(x, y)

func playAnim():
	if velocity == Vector2.ZERO:
		var last_motion = get_last_motion()
		if last_motion.x > 0:
			_animated_sprite.flip_h = false
			_animated_sprite.play("idle")
		else:
			_animated_sprite.flip_h = true
			_animated_sprite.play("idle")
	else:
		if velocity.x > 0:
			_animated_sprite.flip_h = false
			_animated_sprite.play("walk")
		else:
			_animated_sprite.flip_h = true
			_animated_sprite.play("walk")
