extends Node2D

var chicken = preload("res://chicken.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
	pass

func updateWater():
	randomize()
	for cell in $TileMap.get_used_cells(0):
		$TileMap.set_cell(0, cell, 2, Vector2(randi_range(0, 3), 0))

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Input.is_action_just_pressed("click"):
		var instance = chicken.instantiate()
		var mouse_position = get_viewport().get_mouse_position()
		instance.setup(clamp(mouse_position.x, 32, 160), clamp(mouse_position.y, 32, 160))
		add_child(instance)


func _on_timer_timeout():
	updateWater()
