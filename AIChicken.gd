extends Node2D

enum State{
	RELAX,
	PANIC
}
signal state_changed(new_state)
@onready var detection_zone = $DetectionZone

var current_state:int = State.RELAX: set=set_state,get=get_state
var farmer:Farmer =null
var farmer_location=null
var actor:CharacterBody2D=null;
var actor_velocity:Vector2=Vector2.ZERO


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func initialize(actor):
	self.actor=actor


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	match current_state:
		State.RELAX:
			pass
		State.PANIC:
			farmer_location = farmer.get_position()
			actor.velocity=farmer.velocity.normalized()*65
			actor.move_and_slide()
			# print("panic")

func set_state(new_state:int):
	current_state=new_state
	emit_signal("state_changed",current_state)

func get_state():
	return current_state

func set_farmer(body):
	farmer = body
	farmer_location = farmer.get_position()
