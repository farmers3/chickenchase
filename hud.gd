extends CanvasLayer

var score = 0
var farmer

# Called when the node enters the scene tree for the first time.
func _ready():
	for child in get_parent().get_children()[1].get_children():
		if child.name == "Farmer":
			farmer = child
			farmer.connect("chickenDelivered", incrScore)
	pass


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func set_score(newscore):
	print("Score updated")
	score = newscore


func get_score():
	return score


func incrScore():
	set_score(get_score() + 1)
	$Score.text = str(get_score())
