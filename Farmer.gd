extends CharacterBody2D

class_name Farmer

@onready var _animated_sprite = $Sprite
@onready var ai = $AI
var chicken: Chicken=null
var movement_speed: float = 50
var movement_target_position: Vector2 = Vector2(randi_range(50,160),randi_range(50,160))

@onready var navigation_agent: NavigationAgent2D = $NavigationAgent2D

signal chickenDelivered

func _ready():
	_animated_sprite.play("idle_down")
	ai.initialize(self)
	# These values need to be adjusted for the actor's speed
	# and the navigation layout.
	navigation_agent.path_desired_distance = 4.0
	navigation_agent.target_desired_distance = 4.0

	# Make sure to not await during _ready.
	call_deferred("actor_setup")
	
	

func actor_setup():
	# Wait for the first physics frame so the NavigationServer can sync.
	await get_tree().physics_frame

	# Now that the navigation map is no longer empty, set the movement target.
	set_movement_target(movement_target_position)

func set_movement_target(movement_target: Vector2):
	navigation_agent.target_position = movement_target

func _physics_process(delta):
	playAnim()
	if ai.get_state()==0: #CASE PATROL
		if navigation_agent.is_navigation_finished():
			set_movement_target(Vector2(randi_range(50,160),randi_range(50,160)))
		
		
		var current_agent_position: Vector2 = global_position
		var next_path_position: Vector2 = navigation_agent.get_next_path_position()
		
		var new_velocity: Vector2 = next_path_position - current_agent_position
		new_velocity = new_velocity.normalized()
		new_velocity = new_velocity * movement_speed
		
		velocity = new_velocity
		move_and_slide()
#		print("patrol")
	
	if ai.get_state()==1: #CASE ENGAGE
		velocity=Vector2.ZERO
#		print("engage")
	
	if ai.get_state() == 2: # CASE BRING
		var henhouse_position = ai.get_henhouse_position()

		# Vérifier si l'agent a atteint le poulailler
		if navigation_agent.is_navigation_finished():
			print("Arrivé au poulailler")
			emit_signal("chickenDelivered")
			ai.set_state(0)  # Changer l'état de l'agent vers "PATROL"
		
		# Si l'agent n'a pas atteint le poulailler, continuer à aller vers le poulailler
		else:
			set_movement_target(henhouse_position)
			var current_agent_position: Vector2 = global_position
			var next_path_position: Vector2 = navigation_agent.get_next_path_position()
			
			var new_velocity: Vector2 = next_path_position - current_agent_position
			new_velocity = new_velocity.normalized()
			new_velocity = new_velocity * movement_speed
			
			velocity = new_velocity
			move_and_slide()
#		print("bring")


func playAnim():
	if velocity == Vector2.ZERO:
		var last_motion = get_last_motion()
		if abs(last_motion.x) > abs(last_motion.y):
			if last_motion.x > 0:
				_animated_sprite.play("idle_right")
			else:
				_animated_sprite.play("idle_left")
		else:
			if last_motion.y > 0:
				_animated_sprite.play("idle_down")
			else:
				_animated_sprite.play("idle_up")
	else:
		if abs(velocity.x) > abs(velocity.y):
			if velocity.x > 0:
				_animated_sprite.play("walk_right")
			else:
				_animated_sprite.play("walk_left")
		else:
			if velocity.y > 0:
				_animated_sprite.play("walk_down")
			else:
				_animated_sprite.play("walk_up")
