extends Node2D

signal state_changed(new_state)

enum State{
	PATROL,
	ENGAGE,
	BRING,
}

@onready var detection_zone = $DetectionZone

var current_state:int = State.PATROL: set=set_state,get=get_state
var chicken: Chicken=null
var patrol_location:Vector2=Vector2.ZERO
var chicken_location=null
var patrol_location_reached:bool=false
var henhouse_position:Vector2=Vector2(152,64)
var actor:CharacterBody2D=null;
var actor_velocity:Vector2=Vector2.ZERO


func _ready():
	$VisionZone.connect("body_entered", _on_detection_zone_body_entered)

func _process(delta):
	match current_state:
		State.PATROL:
			pass
		State.ENGAGE:
			if chicken!=null:
				chicken_location = chicken.get_position()
				if not patrol_location_reached:
					actor.velocity=actor.global_position.direction_to(chicken_location)*100
					actor.move_and_slide()
					pass
				else:
					actor.velocity=Vector2.ZERO
		State.BRING:
			pass
		_:
			print("Error state not found")
	$VisionZone.look_at(to_global(get_parent().velocity))

func initialize(actor):
	self.actor=actor

func set_state(new_state:int):
	current_state=new_state
	emit_signal("state_changed",current_state)

func get_state():
	return current_state

func _on_detection_zone_body_entered(body:Node):
	if body.is_in_group("chicken") and get_state() == State.PATROL:
		$ObstacleSensor.target_position = to_local(body.global_position)
		if $ObstacleSensor.is_colliding():
			return
		else:
			set_state(State.ENGAGE)
			chicken=body
			chicken_location=chicken.get_position()
			chicken.ai.set_state(1)
			chicken.ai.set_farmer(actor)
			$ExclamationPoint.show()

func _on_detection_zone_body_exited(body):
	if body.is_in_group("chicken") and get_state() == State.ENGAGE and body==chicken:
		set_state(State.PATROL)
		chicken.ai.set_state(0)
		chicken.ai.set_farmer(actor)
		chicken = null
		$ExclamationPoint.hide()

func _on_detection_colision_body_entered(body):
	if chicken and body==chicken:
		print("colliding with chicken")
		patrol_location_reached=true
		set_state(State.BRING)
		chicken.queue_free()
		$ExclamationPoint.hide()
		$ChickenSprite.show()
	else:
		print("colide")

func get_chicken_position():
	return chicken.get_position()

func get_henhouse_position():
	return henhouse_position

func _on_farmer_chicken_delivered():
	print("Poulet livré !")
	patrol_location_reached=false
	$ChickenSprite.hide()
